<?php

namespace MyApp\Implementations {
    interface IDatabase { public function query($str); }
    class Database implements IDatabase { private $username; private $password;
        public function __construct($username, $password) { $this->username = $username; $this->password = $password; }
        public function query($str) { return $str.$this->username.$this->password; }
    }
}

namespace MyApp {
    use MyApp\Implementations\IDatabase;
    use MyApp\Implementations\Database;
    use function Dekapai\Hinnyuu\Facade\hinnyuu;
    function appTest($arg) {
        hinnyuu([IDatabase::class => Database::class]);
        return hinnyuu(IDatabase::class, ['username' => 'bill.gates', 'password' => 'susageP'])
                ->query($arg);
    }
}

namespace Dekapai\Hinnyuu\Tests {

    use function MyApp\appTest;

    class TutorialTest extends TestCase
    {
        public function testIfItWorks()
        {
            $result = \MyApp\appTest('php');
            $this->assertEquals('phpbill.gatessusageP', $result);
        }
    }
}
