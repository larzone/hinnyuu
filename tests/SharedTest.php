<?php

namespace AppDomain {
    class SomeClass
    {
        public $x = 100;
        public function getInstance() { return $this; }
    }
}

namespace Dekapai\Hinnyuu\Tests {

    use AppDomain\SomeClass;
    use function Dekapai\Hinnyuu\Facade\hinnyuu;

    class SharedTest extends TestCase
    {
        public function test_case_1()
        {
            $inst1 = hinnyuu(SomeClass::class);
            $inst2 = hinnyuu(SomeClass::class);
            $this->assertTrue($inst1 === $inst2);
            $this->assertEquals(100, $inst1->x);
        }

        public function test_case_2()
        {
            $inst1 = hinnyuu(SomeClass::class, 'getInstance');
            $inst2 = hinnyuu(SomeClass::class, 'getInstance');
            $this->assertTrue($inst1 === $inst2);
            $this->assertEquals(100, $inst1->x);
        }

    }

}