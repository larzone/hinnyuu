<?php 

namespace Domain1 {

    class A
    {
        public function __construct(B $b, C $c)
        {
            $this->b = $b;
            $this->c = $c;
        }
    }

    class B
    {
        public function __construct(C $c)
        {
            $this->c = $c;
        }
    }

    class C
    {
        public function __construct(D $d)
        {
            $this->d = $d;
        }
    }

    class D
    {
        public function __construct(E $e, F $f)
        {
            $this->e = $e;
            $this->f = $f;
        }
    }

    class E
    {
        public function __construct()
        {
            $this->data = rand(1, 100);
        }
    }

    class F
    {
        public function __construct()
        {
            $this->data = rand(1, 100);
        }
    }
}


namespace Domain2 {
    interface IA
    {
        public function doTheThing();
    }

    class A implements IA
    {
        public $data;

        public function doTheThing()
        {
            $this->data = rand(1, 100);
        }

        public function __construct()
        {
            $this->doTheThing();
        }
    }

    class B
    {
        public function __construct(IA $a)
        {
            $this->a = $a;
        }
    }

    class C
    {
        public function __construct(IA $a)
        {
            $this->a = $a;
        }
    }
}

namespace Domain3 {
    interface IA
    {
        public function doTheThing();
    }

    class ASingleton implements IA
    {
        public $data;
        private static $instance = null;

        public function doTheThing()
        {
            $this->data = rand(1, 100);
        }

        private function __construct()
        {
            $this->doTheThing();
        }

        public static function getInstance()
        {
            if (is_null(static::$instance)) static::$instance = new static();
            return static::$instance;
        }
    }

    class B
    {
        public function __construct(IA $a)
        {
            $this->a = $a;
        }
    }

    class C
    {
        public function __construct(IA $a)
        {
            $this->a = $a;
        }
    }
}

namespace Domain4 {
    interface IA
    {
        public function getB();

        public function getC();
    }

    class ASingleton implements IA
    {
        private $b;
        private $c;
        private static $instance = null;

        private function __construct(B $b, C $c)
        {
            $this->b = $b;
            $this->c = $c;
        }

        public function getB()
        {
            return $this->b;
        }

        public function getC()
        {
            return $this->c;
        }

        public static function getInstance(B $b, C $c)
        {
            if (is_null(static::$instance)) static::$instance = new static($b, $c);
            return static::$instance;
        }
    }

    class B
    {
        public function __construct(C $c)
        {
            $this->c = $c;
        }
    }

    class C
    {
        public $data;

        public function __construct()
        {
            $this->data = rand(1, 100);
        }
    }
}

namespace Domain5 {

    class Object
    {
        public $data;

        public function __construct()
        {
            $this->data = "Default";
        }
    }

    class ObjectFactory
    {
        public static function create()
        {
            $object = new Object();
            $object->data = "Override";
            return $object;
        }
    }

    class A
    {
        public function __construct(Object $object)
        {
            $this->object = $object;
        }
    }

}

namespace Domain7 {
    class A
    {
        public $foo, $bar;

        public function __construct($foo, $bar = 'bar')
        {
            $this->foo = $foo;
            $this->bar = $bar;
        }
    }
}

namespace Domain9
{
    class A
    {
        private $invocations = 0;
        public function get(B $b)
        {
            $this->invocations++;
            if ($this->invocations == 2) throw new \Exception('A:nonononono');
            return $b->showPosts();
        }
    }

    class B
    {
        private $invocations = 0;
        public function showPosts()
        {
            $this->invocations++;
            if ($this->invocations == 2) throw new \Exception('B:nonononono');
            return "ok";
        }
    }
}

namespace Dekapai\Hinnyuu\Tests {

    use function Dekapai\Hinnyuu\Facade\hinnyuu;

    class BasicTest extends TestCase
    {

        /** @test */
        public function test_case_1()
        {
            $a = hinnyuu('Domain1\A');
            $this->assertEquals( $a->b->c->d->e->data, $a->c->d->e->data );
            $this->assertEquals( $a->b->c->d->f->data, $a->c->d->f->data );
        }

        /** @test */
        public function test_case_2()
        {
            hinnyuu(['Domain2\IA' => 'Domain2\A']);
            $b = hinnyuu('Domain2\B');
            $c = hinnyuu('Domain2\C');
            $this->assertEquals( $b->a->data, $c->a->data );
        }

        /** @test */
        public function test_case_3()
        {
            hinnyuu(['Domain3\IA' => ['Domain3\ASingleton', 'getInstance']]);
            $b = hinnyuu('Domain3\B');
            $c = hinnyuu('Domain3\C');
            $this->assertEquals( $b->a->data, $c->a->data );
        }

        /** @test */
        public function test_case_4()
        {
            hinnyuu(['Domain4\IA' => ['Domain4\ASingleton', 'getInstance']]);
            $a = hinnyuu('Domain4\IA');
            $this->assertEquals( $a->getB()->c->data, $a->getC()->data );
        }

        /** @test */
        public function test_case_6()
        {
            hinnyuu(['Dude' => function() { $x = new \stdClass(); $x->foo = 'bar'; return $x; } ]);
            $dude = hinnyuu('Dude');
            $this->assertEquals( $dude->foo, "bar" );
        }

        /** @test */
        public function test_case_7()
        {
            $a = hinnyuu('Domain7\A', ['foo' => 'foo']);
            $this->assertEquals( $a->foo, "foo" );
            $this->assertEquals( $a->bar, "bar" );
        }

        /** @test @expectedException \Exception */
        public function test_case_8()
        {
            $a = hinnyuu('Domain7\A', ['bar' => 'bar']); // not passing required parameter foo
            $this->assertEquals( $a->foo, "foo" );
            $this->assertEquals( $a->bar, "bar" );
        }

        /** @test */
        public function test_case_9()
        {
            $a = hinnyuu('Domain9\A', 'get');
            $this->assertEquals( $a, "ok" );
        }
    }

}
