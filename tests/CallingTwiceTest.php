<?php

namespace DomainLogic {

    class DomainEntity
    {
        private static $n = 0;
        public function __construct()
        {
            static::$n += 1;
        }
        public static function getN() {
            return static::$n;
        }
        public static function reset()
        {
            static::$n = 0;
        }
    }

}

namespace Dekapai\Hinnyuu\Tests {

    use function Dekapai\Hinnyuu\Facade\hinnyuu;
    use DomainLogic\DomainEntity;

    class CallingTwiceTest extends TestCase
    {

        private function getClosure()
        {
            return function () {
                static $x = 0;
                $x++;
                return $x;
            };
        }

        /** @test */
        public function test_case_1()
        {
            // register
            hinnyuu(['dvd' => $this->getClosure()]);
            $this->assertEquals(1, hinnyuu('dvd'));
            $this->assertEquals(1, hinnyuu('dvd'));
        }

        /** @test */
        public function test_case_2()
        {
            // register
            hinnyuu(['dvd' => $this->getClosure()]);
            $this->assertEquals(1, hinnyuu('#dvd'));
            $this->assertEquals(2, hinnyuu('#dvd'));
        }

        /** test */
        public function test_case_3()
        {
            DomainEntity::reset();
            hinnyuu(DomainEntity::class);
            $this->assertEquals(1, DomainEntity::getN());
            hinnyuu(DomainEntity::class);
            $this->assertEquals(1, DomainEntity::getN());
        }

        /** test */
        public function test_case_4()
        {
            DomainEntity::reset();
            hinnyuu('#'.DomainEntity::class);
            $this->assertEquals(1, DomainEntity::getN());
            hinnyuu('#'.DomainEntity::class);
            $this->assertEquals(2, DomainEntity::getN());
        }
    }

}