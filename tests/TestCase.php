<?php namespace Dekapai\Hinnyuu\Tests;

use Dekapai\Hinnyuu\Hinnyuu;

abstract class TestCase extends \PHPUnit_Framework_TestCase
{
    public final function setUp()
    {
        \Dekapai\Hinnyuu\Facade\hinnyuu(Hinnyuu::RESET);
    }
}
