<?php

use function Dekapai\Hinnyuu\Facade\attach;
use function Dekapai\Hinnyuu\Facade\hinnyuu;
use function Dekapai\Hinnyuu\Facade\trigger;

class Dependency {
    private $i = 0;
    public function __construct()
    {
        $this->i = 7;
    }
    public function overrideI()
    {
        $this->i = 19;
    }
    public function getI()
    {
        return $this->i;
    }
}

class Event {
    public function __construct()
    {

    }
}

class Listener {
    public function __construct(Dependency $d) {
        $d->overrideI();
    }
}

class EventTest extends PHPUnit_Framework_TestCase
{
    public function setUp()
    {
        hinnyuu(\Dekapai\Hinnyuu\Hinnyuu::RESET);
    }


    public function test_one_event_one_listener()
    {
        $a = 0;
        attach('event_a', function() use (&$a) { $a = 1; });
        $this->assertEquals(0, $a);
        trigger('event_a');
        $this->assertEquals(1, $a);
    }

    public function test_one_event_two_listeners()
    {
        $a = 0;
        $b = 0;
        attach('event_a', function() use (&$a) { $a = 1; });
        attach('event_a', function() use (&$b) { $b = 1; });
        $this->assertEquals(0, $a);
        $this->assertEquals(0, $b);
        trigger('event_a');
        $this->assertEquals(1, $a);
        $this->assertEquals(1, $b);
    }

    public function test_two_events_one_listener()
    {
        $a = 0;
        $closure = function() use (&$a) { $a++; };
        attach('event_a', $closure);
        attach('event_b', $closure);
        $this->assertEquals(0, $a);
        trigger('event_a');
        $this->assertEquals(1, $a);
        trigger('event_b');
        $this->assertEquals(2, $a);
    }

    public function test_oo()
    {
        $dependency = hinnyuu(Dependency::class);
        attach(Event::class, Listener::class);
        $this->assertEquals(7, $dependency->getI());
        trigger(Event::class);
        $this->assertEquals(19, $dependency->getI());
    }

    public function test_no_listener()
    {
        trigger('crap');
    }

}
