<?php

namespace Domain {

    class Person
    {
        private $name;

        public function __construct($name)
        {
            $this->name = $name;
        }

        public function name()
        {
            return $this->name;
        }
    }

    class PersonFactory
    {
        public static function create($name)
        {
            return new Person($name);
        }
    }

    class Doggy
    {
        private $owner;

        public function __construct(Person $owner)
        {
            $this->owner = $owner;
        }

        public function owner()
        {
            return $this->owner;
        }
    }
}

namespace Dekapai\Hinnyuu\Tests
{

    use function Dekapai\Hinnyuu\Facade\hinnyuu;

    class FactoryTest extends TestCase
    {
        /** @test */
        public function test_case_1()
        {
            $doggy = hinnyuu('Domain\Doggy', ['name' => 'Steve']);
            $this->assertEquals('Steve', $doggy->owner()->name());
        }

        /** @test */
        public function test_case_2()
        {
            hinnyuu(['Domain\Person' => ['Domain\PersonFactory', 'create']]);
            $doggy = hinnyuu('Domain\Doggy', ['name' => 'Steve']);
            $this->assertEquals('Steve', $doggy->owner()->name());
        }

        /** @test */
        public function test_case_3()
        {
            $doggy = hinnyuu('Domain\Doggy', ['Domain\Person@name' => 'Steve']);
            $this->assertEquals('Steve', $doggy->owner()->name());
        }
    }
}
