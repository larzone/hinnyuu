<?php

namespace Domain
{
    class Dependency
    {
        private $var;
        public function __construct($var)
        {
            $this->var = $var;
        }
        public function getVar()
        {
            return $this->var;
        }
    }

    abstract class BaseClass {
        protected $x;
        public function __construct()
        {
            $this->x = 100;
        }
    }

    class DerivedClass extends BaseClass
    {
        public function getX()
        {
            return $this->x;
        }
    }
}

namespace Dekapai\Hinnyuu\Tests
{

    use function Dekapai\Hinnyuu\Facade\hinnyuu;
    use Domain\Dependency;
    use Domain\DerivedClass;

    class DependencyTest extends TestCase
    {
        /** @test */
        public function test_case_1()
        {
            $closure = function(Dependency $x) { return $x->getVar(); };
            $result = hinnyuu($closure, ['var' => 5]);
            $this->assertEquals(5, $result);
        }

        /** @test */
        public function test_case_2()
        {
            $closure = function() { return "hello"; };
            $result = hinnyuu($closure);
            $this->assertEquals("hello", $result);
        }

        /** @test */
        public function test_case_3()
        {
            $closure = function(DerivedClass $c) { return $c->getX(); };
            $result = hinnyuu($closure);
            $this->assertEquals(100, $result);
        }
    }
}
