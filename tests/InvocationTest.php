<?php

namespace Domain
{
    class Fish
    {
        private $bar;
        public function __construct($bar)
        {
            $this->bar = $bar;
        }
        public function getBar()
        {
            return $this->bar;
        }
    }

    class Bird
    {
        private $foo;
        public function __construct($foo)
        {
            $this->foo = $foo;
        }
        public function getFoo()
        {
            return $this->foo;
        }
    }


    class Entity
    {
        public function __construct()
        {

        }

        public function controllerMethod(Fish $fish, Bird $bird, $baz)
        {
            return $fish->getBar() * $bird->getFoo() + $baz;
        }
    }
}

namespace Dekapai\Hinyuu\Tests
{

    use function Dekapai\Hinnyuu\Facade\hinnyuu;
    use Dekapai\Hinnyuu\Tests\TestCase;

    class InvocationTest extends TestCase
    {
        public function test_case_1()
        {
            $result = hinnyuu('Domain\Entity', 'controllerMethod', ['foo' => 5, 'bar' => 6, 'baz' => 7]);
            $this->assertEquals(37, $result);
        }

        /** @expectedException \Exception */
        public function test_case_2()
        {
            $result = hinnyuu('Domain\Entity', 'controllerMethod', ['bar' => 6, 'baz' => 7]);
            $this->assertEquals(37, $result);
        }
    }
}
