<?php namespace Dekapai\Hinnyuu;

use ReflectionClass;
use ReflectionFunction;
use ReflectionMethod;
use ReflectionParameter;

class Hinnyuu
{
    private $serviceList;
    private $instantiated = array();
    private $namedParameters;    // default values to give parameters if they are not instantiable

    const RESET = 0xBADC0FFEE;

    /**
     * @var Profiler
     */
    private $profiler = null;

    public function __construct(array $serviceList = array())
    {
        $this->serviceList = $serviceList;
    }

    public function setProfiler(Profiler $profiler)
    {
        $this->profiler = $profiler;
    }

    private function memoizeWrapper($className, $methodName, callable $callback)
    {
        if (isset($this->instantiated[$className])) {
            return $this->instantiated[$className];
        }
        if (!is_null($this->profiler)) $this->profiler->startTimer("instantiate $className");
        $instance = $callback($className, $methodName);
        $this->instantiated[$className] = $instance;
        if (!is_null($this->profiler)) $this->profiler->stopTimer("instantiate $className");
        return $instance;
    }

    private function getInstanceFromServiceList($className)
    {
        if (!is_array($this->serviceList[$className])) { // so it must be callable
            return $this->serviceList[$className]();
        }
        if ( count($this->serviceList[$className]) == 2 ) {
            list($className, $methodName) = $this->serviceList[$className];
            $reflectionClass = new ReflectionClass($className);
            $instance = $reflectionClass->newInstanceWithoutConstructor();
            $reflectionMethod = new ReflectionMethod($className, $methodName);
            return $reflectionMethod->invokeArgs($instance, $this->getMethodParameters($reflectionMethod, $className));
        }
        if (count($this->serviceList[$className]) == 1) {
            return $this->getInstance($this->serviceList[$className][0]);
        } else if (count($this->serviceList[$className]) == 2) {
            return $this->getInstance($this->serviceList[$className][0], $this->serviceList[$className][1]);
        } else {
            throw new \Exception("Must have 1 or 2 elements in array, className = {$className}");
        }
    }

    private function instantiate(ReflectionParameter $reflectionParameter, $instantiatingClassName)
    {
        $instance = null;
        $isDefaultValueAvailable = $reflectionParameter->isOptional() && $reflectionParameter->isDefaultValueAvailable();
        $name = $reflectionParameter->getName();
        if (!is_null($this->namedParameters) && in_array($name, array_keys($this->namedParameters))) {
            $pattern = $this->namedParameters[$name][0];
            if ($pattern == '*' || $pattern == $instantiatingClassName) {
                return $this->namedParameters[$name][1];
            }
        }
        $class = $reflectionParameter->getClass();
        if (is_null($class)) {
            if ($isDefaultValueAvailable) {
                return $reflectionParameter->getDefaultValue();
            } else {
                throw new \Exception("{$name} is not a class and has no default parameter value");
            }
        } else {
            if ($reflectionParameter->getClass()->isInterface()) {
                $that = &$this;
                return $this->memoizeWrapper(
                    $reflectionParameter->getClass()->name,
                    '__construct',
                    function($cn) use ($that) { return $that->getInstanceFromServiceList($cn); }
                );
            } else {
                $className = $reflectionParameter->getClass()->name;
                return $this->getInstance($className);
            }
        }
    }

    private function getMethodParameters(ReflectionMethod $reflectionMethod, $instantiatingClassName)
    {
        $constructorParams = array();
        foreach ($reflectionMethod->getParameters() as $parameter) {
            $constructorParams[] = $this->instantiate($parameter, $instantiatingClassName);
        }
        return $constructorParams;
    }

    private function _getInstance($className, $methodName)
    {
        if (isset($this->serviceList[$className])) {
            $instance = $this->getInstanceFromServiceList($className);
        } else {

            $refClass = new \ReflectionClass($className);
            if (!$refClass->isInstantiable()) throw new \Exception("{$className} cannot be instantiated.");

            if ($methodName === '__construct' && $refClass->hasMethod($methodName)) {
                $refMethod = new \ReflectionMethod($className, $methodName);
                $instance = $refClass->newInstanceArgs($this->getMethodParameters($refMethod, $className));
            } else {

                if ($refClass->hasMethod('__construct')) { // if constructor exists run it before the requested method
                    $refMethod = new \ReflectionMethod($className, '__construct');
                    $instance = $refClass->newInstanceArgs($this->getMethodParameters($refMethod, $className));
                } else {
                    $instance = $refClass->newInstance();
                }
                if ($methodName !== '__construct') {
                    $refMethod = new \ReflectionMethod($instance, $methodName);
                    $instance = $refMethod->invokeArgs($instance, $this->getMethodParameters($refMethod, $className));
                }
            }
        }

        return $instance;
    }

    private function parse($namedParameters)
    {
        // [name => ['*', value] ]
        $retVal = array();
        foreach ($namedParameters as $name => $value) {
            if (strpos($name, '@')) {
                list($pattern, $name) = explode('@', $name);
            } else {
                $pattern = '*';
            }
            $retVal[$name] = array($pattern, $value);
        }
        return $retVal;
    }

    public function getInstance($className, $methodName = '__construct', array $namedParameters = null)
    {
        if (!is_null($namedParameters)) $this->namedParameters = $this->parse($namedParameters);
        if (!is_string($methodName)) throw new \Exception(sprintf("Method name must be a string, is: %s.", json_encode($methodName)));
        if ($className{0} == '#') {
            return $this->_getInstance(substr($className, 1), $methodName);
        } else {
            $instance = $this->memoizeWrapper($className, $methodName, function($cn, $mn) { return $this->_getInstance($cn, $mn); });
        }
        return $instance;
    }

    public function registerService($alias, $target)
    {
        if (!(is_string($alias) && strlen($alias))) throw new \Exception("Alias must be a string.");
        if (!(is_callable($target) || is_array($target))) throw new \Exception("Target must be callable or an array.");
        if (is_array($target) && (count($target) < 1 || count($target) > 2)) throw new \Exception("Target must have length 1 or 2 (or be callable).");
        $this->serviceList[$alias] = $target;
    }

    public function invoke($className, $namedParameters = null)
    {
        if (is_callable($className) && !is_string($className) && !is_array($className)) {
            if (!is_null($namedParameters)) $this->namedParameters = $this->parse($namedParameters); // TODO fix code duplicated above
            $params = $this->getClosureParameters(new ReflectionFunction($className));
            return call_user_func_array($className, $params);
        }
        throw new \InvalidArgumentException("Invalid argument type.", 1008);
    }

    private function getClosureParameters(ReflectionFunction $reflectionFunction)
    {
        $functionParams = array();
        foreach ($reflectionFunction->getParameters() as $parameter) {
            $functionParams[] = $this->instantiate($parameter, '');
        }
        return $functionParams;
    }
}
