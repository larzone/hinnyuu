<?php namespace Dekapai\Hinnyuu;

interface Profiler
{
    public function startTimer($timer);
    public function stopTimer($timer);
    public function getFileName($key);
}
