<?php namespace Dekapai\Hinnyuu\Facade;

use Dekapai\Hinnyuu\Hinnyuu;
use Dekapai\Hinnyuu\Profiler;

function hinnyuu($arg, $arg2 = null, $arg3 = null)
{
    static $hinnyuu = null;

    if (is_int($arg)) {
        switch ($arg) {
            case Hinnyuu::RESET:
                $hinnyuu = null;
                return null;
            default:
                throw new \InvalidArgumentException("Invalid code", 1010);
        }
    }

    if (empty($hinnyuu)) {
        $hinnyuu = new Hinnyuu();
    }

    if ($arg instanceof Profiler) {
        $hinnyuu->setProfiler($arg);
        return null;
    }

    if (is_array($arg) && !isset($arg[0])) {    // register

        foreach ($arg as $key => $value) {
            if (is_string($value)) {
                $hinnyuu->registerService($key, [$value]);
            } else if (is_array($value) && isset($value[0]) && isset($value[1]) && count($value) == 2) {
                $hinnyuu->registerService($key, $value);
            } else if (is_callable($value)) {
                $hinnyuu->registerService($key, $value);
            } else {
                throw new \InvalidArgumentException("Invalid value passed to hinnyuu.", 1001);
            }
        }
        return null;

    } else if (is_string($arg) || is_callable($arg)) {   // get

        // order of arg2 and arg3 is unimportant, infer intent from type
        foreach ([$arg2, $arg3] as $a)
            if (!is_null($a)) if (!is_string($a) && !is_array($a))
                throw new \InvalidArgumentException("Invalid parameter type.", 1002);

        // if one argument is string the other must be array, and vice versa
        if (!is_null($arg2) && !is_null($arg3)) if (is_array($arg2) && is_array($arg3) || is_string($arg2) && is_string($arg3))
            throw new \InvalidArgumentException("Invalid parameter type.", 1004);

        if (is_null($arg2) && is_null($arg3)) {
            if (is_string($arg) && !is_callable($arg)) return $hinnyuu->getInstance($arg);
            else return $hinnyuu->invoke($arg);
        }

        $namedParameters = [];
        $methodName = '__construct';

        foreach ([$arg2, $arg3] as $a)
            if (!is_null($a)) {
                if (is_string($a)) $methodName = $a;
                if (is_array($a)) $namedParameters = $a;
            }

        if (is_string($arg) && !is_callable($arg)) return $hinnyuu->getInstance($arg, $methodName, $namedParameters);
        else return $hinnyuu->invoke($arg, $namedParameters);

    } else {

        throw new \InvalidArgumentException("Invalid value passed to hinnyuu.", 1003);

    }

}

function attach(string $eventClass, $listener = null)
{
    static $eventsDict = [];
    if (is_null($listener)) {
        array_map(function($listener) { hinnyuu($listener); }, $eventsDict[$eventClass] ?? []);
    } else {
        $eventsDict[$eventClass][] = $listener;
    }
}

function trigger(string $eventClass)
{
    attach($eventClass, null);
}
